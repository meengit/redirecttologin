README.txt for Redirect after Login Module
------------------------------------------

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

The Redirect After Login Module is a simple Drupal 8 module to redirect unauthenticated users to the Login page (`/user/login`).

It hooks very early into the user status using an event subscriber that subscribes to the `KernelEvents::REQUEST`.

For unauthenticated users, the module returns with an HTTP status 302 (`FOUND`) and redirects to the login page. If the user, later on, hits the same URL in an authenticated state, the browser knows it has to fetch the page again and does not load the login form (again).

The module initially based on the post ["Redirect Anonymous user to login page at StackExchange."](https://drupal.stackexchange.com/questions/223095/redirect-anonymous-user-to-login-page?noredirect=1&lq=1)

REQUIREMENTS
------------

No requirements.

RECOMMENDED MODULES
-------------------

No recommended modules.

INSTALLATION
------------

This module is currently not listed at [drupal.org](https://www.drupal.org/). I suggest an installation into the `modules/custom` directory using git clone:

```bash
git clone https://gitlab.com/meengit/redirecttologin.git
```

CONFIGURATION
-------------

No configuration, yust enable to use.

TROUBLESHOOTING
---------------

Nothing known.

FAQ
---

Nothing known.

MAINTAINERS
-----------

* Andreas, <https://gitlab.com/meengit/redirecttologin>

