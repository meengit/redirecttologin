<?php

namespace Drupal\redirecttologin\EventSubscriber;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber subscribing to KernelEvents::REQUEST.
 */
class RedirectAnonymousSubscriber implements EventSubscriberInterface {

  /**
   * Holds the redirect code.
   *
   * @var int
   */
  private $redirectCode = 302;

  /**
   * Checks if the user is logged in.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   GetResponseEvent.
   */
  public function redirecttologin(GetResponseEvent $event) {
    $account = \Drupal::currentUser();
    $route = \Drupal::routeMatch()->getRouteName();
    if (empty($account->id()) && $route != 'user.login') {
      $response = new RedirectResponse('/user/login', $this->redirectCode);
      $response->send();
      exit(0);
    }
  }

  /**
   * Gets the subscribed events.
   *
   * @return Event
   *   The subscribed events.
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['redirecttologin'];
    return $events;
  }

}
